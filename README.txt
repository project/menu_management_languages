
-- SUMMARY --

* This is a super light and simple module to improve the usability of the menu
 management administration interface in multilingual sites.

* When you're building a multilingual site, if you have a taxonomy vocabulary
 with terms in different languages and you edit the terms of that vocabulary
 (Admin > Structure > Taxonomy, then click on the "list terms" link of one
 multilingual vocabulary), you can see the language of each term next to it.

* But, if you have a multilingual menu with menu items in different languages,
 when you edit the menu items of that menu (Admin > Structure > Menus, then
 click on the "list links" link of one multilingual menu), you can't see in
 which language each menu item is. This can go even worse if the translation
 of the menu item is the same for different languages. This makes the
 managament of these menus pretty hard.

* This module tries to fix this problem, adding the language name each menu
 item belongs to next to the menu item in the administration interface. You
 can see what this module does in the attached screenshot.

-- REQUIREMENTS --

This module requires the following modules:
* Drupal's core menu module
* Menu Translation module (i18n_menu). It's a submodule of the i18n module
 (https://www.drupal.org/project/i18n).

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* No configuration needed. Install the module and you're done.

-- CUSTOMIZATION --

-- TROUBLESHOOTING --

-- FAQ --

-- CONTACT --

Current maintainers:
* Ángel Fariña (GoddamnNoise) - http://www.drupal.org/user/795168
* David Cereijo (dcereijo)    - http://www.drupal.org/user/1108366

This project has been sponsored by:
* Karakana Factoría TIC
  Spanish software development shop specialized in consulting and planning 
  of Drupal powered sites, Karakana Factoría TIC offers installation, 
  development, theming, customization, and hosting to get you started. 
  Visit http://www.karakana.es for more information.
